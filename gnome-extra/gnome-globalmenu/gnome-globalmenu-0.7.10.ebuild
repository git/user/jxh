# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="3"
GCONF_DEBUG="no"

inherit autotools eutils gnome2

MY_P=${P/_p/_}

DESCRIPTION="Global menubar applet for Gnome2."
HOMEPAGE="http://code.google.com/p/gnome2-globalmenu/"
SRC_URI="http://gnome2-globalmenu.googlecode.com/files/${MY_P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE="gnome +introspection xfce"

RDEPEND=">=x11-libs/gtk+-2.10:2
	>=dev-libs/glib-2.10:2
	gnome-base/gconf:2
	>=x11-libs/libwnck-2.16:1
	>=gnome-base/gnome-menus-2.16
	>=x11-libs/libX11-1.1.0
	gnome? (
		>=gnome-base/gnome-panel-2.16
		>=x11-libs/libnotify-0.4.4 )
	xfce? (
		>=xfce-base/xfce4-panel-4.4.3 )
"
DEPEND="${RDEPEND}
	dev-util/intltool
	dev-util/pkgconfig
	>=dev-lang/vala-0.7.7:0.10"

S="${WORKDIR}/${MY_P}"

pkg_setup() {
	DOCS="AUTHORS ChangeLog NEWS README*"
	G2CONF="${G2CONF}
		--docdir=/usr/share/doc/${PF}
		--without-gir
		$(use_with gnome gnome-panel)
		$(use_with xfce xfce4-panel)
		VALAC_BIN=$(type -P valac-0.10)"
		# Does not enable anything
		#$(use_with introspection gir)
}

src_prepare() {
	gnome2_src_prepare

	# Append missing files
	cat >>po/POTFILES.in <<EOF
applet/applet.c
applet/application.c
applet/gtkextra-gconfdialog.c
applet/main.c
applet/switcher.c
applet/workspaceselector.c
globalmenu-plugin/module-main.c
libserver/globalmenuitem.c
libserver/widgets/menuitem.c
tools/globalmenu-settings.c
EOF

}

src_install() {
	gnome2_src_install
	find "${ED}" -name "*.la" -delete
}
