# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Synchronization for GNOME"
HOMEPAGE="http://www.conduit-project.org"
SRC_URI="http://files.conduit-project.org/releases/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE="evolution"

DEPEND=">=dev-python/pygoocanvas-0.8.0
		>=dev-python/vobject-0.4.8
		>=dev-python/pyxml-0.8.4
		>=dev-python/pygtk-2.10.3
		>=dev-python/pysqlite-2.3.1
		evolution? ( >=dev-python/evolution-python-0.0.3 )"
RDEPEND=">=dev-python/pygoocanvas-0.8.0"

src_compile() {
	if ! use evolution ; then
			econf --enable-ecal=no
		else
			econf
		fi
}

src_install() {
	emake DESTDIR="${D}" install || die "Error installing ${PN}"
}
