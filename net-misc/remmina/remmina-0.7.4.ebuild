# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

inherit autotools gnome2 eutils

DESCRIPTION="Gtk+/GNOME Remote Desktop Client"
HOMEPAGE="http://remmina.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"

IUSE="avahi ssh vnc"

RDEPEND=">=x11-libs/gtk+-2.16
	>=dev-libs/glib-2.20
	net-misc/rdesktop
	>=dev-libs/libgcrypt-1.4.0
	>=dev-libs/libunique-1.0.8
	>=x11-libs/vte-0.12
	avahi? ( >=net-dns/avahi-0.6.2[gtk,dbus] )
	ssh? ( net-misc/openssh 
               >=net-libs/libssh-0.4 )
	vnc? ( net-misc/tigervnc
		sys-libs/zlib
		media-libs/jpeg
		>=net-libs/gnutls-2.4 )"

DEPEND="${RDEPEND}"

docs="AUTHORS ChangeLog NEWS README"

pkg_setup() {
	G2CONF="${G2CONF}
		$(use_enable avahi)
		$(use_enable ssh)
		$(use_enable vnc)
		--enable-gcrypt --enable-vte"
}
