# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="ALE (Anti-Lamenessing Engine) is an image-processing program used for tasks such as image mosaicking, super-resolution, deblurring, noise reduction, anti-aliasing, and scene reconstruction."
HOMEPAGE="http://auricle.dyndns.org/ALE/"
SRC_URI="http://auricle.dyndns.org/ALE/download/${P}.tar.gz"
LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86 ~ppc ~amd64"
IUSE="fftw imagemagick doc"
RDEPEND="fftw? ( =sci-libs/fftw-3* )
	       imagemagick? ( media-gfx/imagemagick )"
DEPEND="${RDEPEND}
	doc? ( app-text/docbook-sgml-utils )"

src_compile() {
	      econf $(use_with imagemagick) $(use_with fftw fftw3) --enable-wmx \
	      	    || die "econf failed"
		    emake || die "emake failed"
		    use doc && (emake -C doc chunk || die "emake chunk failed")
}


src_install() {
	      emake install DESTDIR="${D}" || die "emake install failed"
	      if use doc; then
	      	 # I don't know why it creates a .html for index, but it's bad
		   cp -f doc/index/.html doc/index/index.html
		      dohtml doc/index/* || die "dohtml failed"
		      fi
}
