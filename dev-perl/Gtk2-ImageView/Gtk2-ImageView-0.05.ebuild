# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

MODULE_AUTHOR=RATCLIFFE
inherit perl-module

DESCRIPTION="Perl binding for the GtkImageView image viewer widget."

SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="test"

SRC_TEST="do"

COMMON="dev-lang/perl
	>=media-gfx/gtkimageview-1.6.3"
RDEPEND="${COMMON}"
DEPEND="dev-perl/glib-perl
	dev-perl/gtk2-perl
	${COMMON}"
